// prepare url parsing & mvc structure & usings
require('./renderer.js')
const body       = $('body')
const config     = require('./config.json')
const resolveApp = require('./src/js/resolve_path.js')
const notificationManager = require('electron-notifications')

// G is for (g)lobal, F is for (f)ragments
var G = F = {}

// Firebase declaration
const firebaseInstance = firebase.initializeApp(config.firebase)
const storage = firebaseInstance.storage()

const storageRef = storage.ref()

resolveApp(__dirname)

window.onhashchange = x => {
    let title = document.querySelector('title')
    title.innerText = config.app_name

    resolveApp(__dirname)
}