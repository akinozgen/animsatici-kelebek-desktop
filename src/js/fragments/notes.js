const template = require('../helpers/template')
const noteParser = require('../helpers/note_parser')
const base64 = require('base64-url')
const Masonry = require('masonry-layout')
const jQueryBridget = require('jquery-bridget')

module.exports = {
	
	get: (fragmentElement) => {
		
		fragmentElement.load(config.fragmentViews + '/notes.html', () => {
			let searchForm = $('.search-form'),
				activeSearchForm = $('[data-activates=search-form]'),
				refreshButton = $('[data-activates="refresh"]')
			let notes = $('.note')
			let deleteNoteConfirm = $('#deleteNoteConfirm'),
				editNoteModal = $('#editNoteModal'),
				shareNoteModal = $('#shareNoteModal')
			let datePicker = $('.datepicker'), timePicker = $('.timepicker'), select = $('select')
			let gridLayout = $('#content')
			
			jQueryBridget('masonry', Masonry, $)
			gridLayout.masonry({
				itemSelector: '.col.s12'
			})
			
			notes.hover((root) => {
				var $this = $(root.currentTarget)

				if ($this.hasClass('z-depth-5'))
					$this.removeClass('z-depth-5')
				else
					$this.addClass('z-depth-5')
			})//endOfHoverEffect

			activeSearchForm.click(() => {
				let state = searchForm.hasClass('hide')
				
				if ( ! state) {
					searchForm.addClass('hide')
				} else {
					searchForm.removeClass('hide')
				}
			})//endOfActiveSearch

			deleteNoteConfirm.modal({
				dismissable: false,
				opacity: .5,
				inDuraton: 500,
				outDuration: 300,
				ready: (modal, trigger) => {
					$noteData = $(trigger)
					let deleteNoteConfirmModalAccept = $('.deleteNoteConfirmModalAccept')
					deleteNoteConfirmModalAccept.data('note-id', $noteData.data('id'))
					deleteNoteConfirmModalAccept.data('owner-id', $noteData.data('owner-id'))
				}
			})//deleteNoteConfirmModal declaration

			editNoteModal.modal({
				ready: F.Notes.fillEditForm
			})//editNoteModal declaration

			shareNoteModal.modal({
				ready: F.Notes.getFriends
			})

			refreshButton.click(F.Notes.getNotes)//refreshButton onClick declaration

			select.material_select()//materialSelect declaration
			datePicker.pickadate({
				format: 'yyyy-mm-dd'
			})//datePicker declaration
			timePicker.pickatime({
				default: 'now',
				twelvehour: true,
				donetext: 'OK',
				autoclose: false,
				vibrate: true,
				twelvehour: false
			})//timePicker declaration
		})
	},

	getNotes: () => {
		let loggedUser = JSON.parse(localStorage.getItem('user'))

		$.ajax({
			url: config.url + 'Notes/get_notes/referer/' + config.referer + '/userid/' + loggedUser.Kimlik + '/table/' + loggedUser.NotlarTablosu,
			type: 'get',
			success: (data) => {
				if (data.result == 'error_cant') {
					Materialize.toast('Notlar yüklenemedi.', 2000)
				} else if (data.result == 'error_not') {
					Materialize.toast('Hiç notunuz yok.', 2000)
				} else if (data.result == 'success') {
					
					localStorage.setItem('notes', JSON.stringify(data.data))
					let content = ""

					for (let i = 0; i < data.data.length; i++) {
						let tmp  = $('template[name=note]').html()
						data.data[i] = noteParser.parse(data.data[i])
						let appendedItem = $(template.replace(tmp, data.data[i]))

						if (data.data[i].Sahibi != loggedUser.Kimlik) {
							appendedItem.find('[data-action="share"]').remove()
						}

						content += appendedItem.prop('outerHTML')
					}

					if (content == "") {
						content = '<center style="color: #e5e5e5">Hiç notunuz yok</center>'
					}

					let notes = $('.notes')
					notes.children('.row').html(content)

					let tooltipped = $('.tooltipped')

					tooltipped.tooltip()
				}
			}
		})//endOfGetNotes
	},

	removeNote: el => {
		let url = ""
		let element = $(el)
		let noteId = element.data('note-id'), ownerId = element.data('owner-id')
		let loggedUser = JSON.parse(localStorage.getItem('user'))

		if (loggedUser.Kimlik == ownerId) {
			url = config.url + 'Notes/delete_note/id/' + noteId + '/table/' + loggedUser.NotlarTablosu +
				  '/referer/' + config.referer
		} else {
			url = config.url + 'Notes/unshare_note/userid/' + loggedUser.Kimlik + '/noteid/' + noteId +
				  '/table/notlar_1/referer/' + config.referer
		}

		$.ajax({
			url: url,
			type: 'get',
			async: false,
			success: data => {
				if (data.result == 'error_cant' || data.result == 'error_not') {
					Materialize.toast('Not silinemedi.', 2000)
				} else if (data.result == 'success') {
					Materialize.toast('Not silindi', 2000)
					$('.note-' + noteId).remove()
				}
			}
		})
	},

	fillEditForm: (modal, trigger) => {
		$noteData = $(trigger)
		
		$notes = JSON.parse(localStorage.getItem('notes'))
		$note = $.grep($notes, e => {
			return e.Kimlik == $noteData.data('id')
		})[0]
		let editNoteConfirmModalAccept = $('.editNoteConfirmModalAccept')
		editNoteConfirmModalAccept.attr('data-action', $noteData.data('action'))
		editNoteConfirmModalAccept.data('action', $noteData.data('action'))

		// Form Elements
		let noteFormTitle = $('#noteFormTitle'), title = $('#title'), context = $('#context'), remind = $('#remind'),
			reminderDate = $('#reminderDate'), reminderTime = $('#reminderTime'),
			color = $('#color'), noteid = $('#noteid')

		if ($noteData.data('action') == 'add') {
			noteFormTitle.text('Ekle')
		} else {
			noteFormTitle.text('Düzenle')
		}

		noteid.val($note.Kimlik)
		title.val($note.Baslik)
		context.val($note.Icerik)
		
		if ($note.Hatirlat) {
			remind.attr('checked', 1)
			remind.data('checked', true)
			reminderDate.parent().show()
			reminderTime.parent().show()
		} else {
			remind.removeAttr('checked')
			remind.data('checked', false)
			reminderDate.parent().hide()
			reminderTime.parent().hide()
		}

		remind.change((el) => {
			if ($(el.currentTarget).is(':checked')) {
				reminderDate.parent().show()
				reminderTime.parent().show()
			} else {
				reminderDate.parent().hide()
				reminderTime.parent().hide()
			}
		})

		reminderDate.val($note.Hatirlatici.split(' ')[0])
		reminderTime.val($note.Hatirlatici.split(' ')[1])

		let $options = color.children('option')
		$($options[parseInt($note.Renk)]).prop('selected', true)
		color.material_select()

		editNoteConfirmModalAccept.attr('data-note-id', $noteData.data('id'))
		editNoteConfirmModalAccept.data('note-id', $noteData.data('id'))
	},

	getFriends: (modal, trigger) => {
		let $noteData = $(trigger)
		let loggedUser = JSON.parse(localStorage.getItem('user'))
		let url = config.url + 'UserRelations/get/userid/' +
				  loggedUser.Kimlik + 
				  '/noteid/' + $noteData.data('id') + 
				  '/referer/' + config.referer
		
		$.ajax({
			url: url,
			type: 'get',
			async: true,
			success: data => {
				if (data.result == 'error_cant') {
					Materialize.toast('Arkadaşlar yüklenemedi.', 2000)
					data = []
				} else if (data.result == 'error_not') {
					Materialize.toast('Hiç arkadaşınız yok.', 2000)
					data = []
				} else if (data.result == 'success') {
					localStorage.setItem('friends', JSON.stringify(data.data))
					F.Notes.getFriendsToModal()
					return
				}
				localStorage.setItem('friends', JSON.stringify(data.data))
			}
		})

		$(modal).attr('data-id', $noteData.data('id'))
		$(modal).data('id', $noteData.data('id'))
	},

	getFriendsToModal: () => {
		let context         = "",
			friendsTemplate = $('#friends'),
			itemTemplate    = $('template[name=friend-collection-item-template]')
		let friends = JSON.parse(localStorage.getItem('friends'))
		if (friends.length > 0) {

			for (let i = 0; i < friends.length; i++) {
				let appendedItem = $(template.replace(itemTemplate.html(), friends[i]))
				
				if (friends[i].Paylasilmis) {
					appendedItem.find('input[type=checkbox]').attr('checked', 'checked')
				}

				context += appendedItem.prop('outerHTML')
			}
			
			friendsTemplate.html($(context))

		} else {
			friendsTemplate.html('Hiç arkadaşınız yok.')
		}
	},

	shareNote: el => {
		let friends = $('#' + $(el).data('container')).find('[name="shareTo[]"]')
		let modal = $('#' + $(el).data('container')).parent().parent()
		let selectedFriends = []

		for (let i = 0; i  < friends.length; i++) {
			if ($(friends[i]).is(':checked')) {
				selectedFriends.push($(friends[i]).val())
			}
		}
		
		$.ajax({
			url: config.url + 'Notes/share_note/userids/' +
				 JSON.stringify(selectedFriends) +
				 '/noteid/' + modal.data('id') + 
				 '/referer/' + config.referer,
			type: 'get',
			success: data => {
				if (data.result == 'error_cant' || data.result == 'error_not') {
					Materialize.toast('Not paylaşılamadı.', 2000)
				} else if (data.result == 'success') {
					Materialize.toast('Paylaşımlar kaydedildi.', 2000)
					F.Notes.getNotes()
				}
			}
		})
	},

	saveNote: el => {
		let container      = $('#' + $(el).data('container')),
			action         = $(el).data('action') == 'add' ? 'add_note' : 'save_note',
			title          = container.find('#title').val(),
			context        = container.find('#context').val(),
			remind         = container.find('#remind').is(':checked'),
			reminderDate   = container.find('#reminderDate').val() ? container.find('#reminderDate').val() : '0000-00-00',
			reminderTime   = container.find('#reminderTime').val() ? container.find('#reminderTime').val() : '00:00:00',
			color          = container.find('#color').val()
			noteid         = container.find('#noteid').val() ? container.find('#noteid').val() : '0',
		    loggedUser     = JSON.parse(localStorage.getItem('user')),
			encodedContent = base64.encode(context), encodedTitle = base64.encode(title),
			userid         = action == 'add_note' ? loggedUser.Kimlik : $note.Sahibi,
			url            = config.url + 'Notes/'+ action +'/userid/' + userid + '/title/' + 
				  encodedTitle + '/content/' + encodedContent + '/remind/' + (remind + 0) + '/reminder/' + 
				  base64.escape(reminderDate + ' ' + reminderTime) + '/color/' + color + '/referer/' + config.referer + 
				  '/table/' + loggedUser.NotlarTablosu + '/id/' + noteid

		$.ajax({
			url: url,
			type: 'get',
			success: data => {
				if (data.result == 'error_cant' || data.result == 'error_not') {
					Materialize.toast('Not kaydedilemedi.', 2000)
				} else if (data.result == 'success') {
					Materialize.toast('Not kaydedildi.', 2000)
					F.Notes.getNotes()
				}
			}
		})

		container.find('#title').val('') 
		container.find('#context').val('') 
		container.find('#remind').val('') 
		container.find('#reminderDate').val('')
		container.find('#reminderTime').val('')
		container.find('#noteid').val('')
	}
}