const template = require('../helpers/template')
const pathinfo = require('pathinfo')
const base64   = require('base64-url')

module.exports = {
	/**
	 * @var jQuery fragmentElement
	 */
	get: (fragmentElement) => {
		fragmentElement.load(config.fragmentViews + '/settings.html', element => {
			
			let context          = $('.fragmentContext')
			let settingsTemplate = $('template[name=settings-form]')
			let loggedUser       = JSON.parse(localStorage.getItem('user'))

			let appendItem       = template.replace(settingsTemplate.html(), loggedUser)

			context.append(appendItem)

			let ppImg = $('input.pp')
			ppImg.change((el) => {
				F.Settings.changePP(el.currentTarget)
			})
		})
	},

	changePP: $this => {
		Materialize.toast('Lütfen bekleyin. Profil resmi birazdan yüklenecek...', 1000)
		if ( ! $this.files) 
			return;

		let
			loggedUser = JSON.parse(localStorage.getItem('user')),
			file       = $this.files[0],
			info       = pathinfo(file.path),
			uploadName = loggedUser.Kimlik + info.filename,
			img        = $('img.pp'),
			reader     = new FileReader()

		reader.onload = e => {
			img.attr('src', e.target.result)
		}

		reader.readAsDataURL(file)

		let myPpRef = storageRef.child('profile_images/' + uploadName)

		myPpRef.put(file, {
			contentType: 'image/jpeg'
		}).then(snapShot => {
			if (snapShot.f == 'success') {
				Materialize.toast('Profil resmi güncellendi.', 2000)
				F.Settings.updatePP(snapShot)
			} else {
				Materialize.toast('Profil resmi güncellenemedi.', 200)
			}
		})
	},

	updatePP: snap => {
		let loggedUser = JSON.parse(localStorage.getItem('user'))
			loggedUser.Avatar = snap.downloadURL != '' ? snap.downloadURL : loggedUser.Avatar
		
		localStorage.setItem('user', JSON.stringify(loggedUser))

		let url = config.url + 'User/save_pp/userid/' + loggedUser.Kimlik + '/referer/' +
				  config.referer + '/newpp/' + base64.encode(snap.downloadURL).replace('/', 'SLASH')
		$.ajax({
			url: url,
			type: 'get',
			async: true,
			success: data => {
				if (data.result == 'error_cant' && data.result == 'error_not') {
					Materialize.toast('Profil resmi güncellenemedi.', 2000)
				} else if (data.result == 'success') {
					Materialize.toast('Profil resminiz sunucuya kaydedildi.', 2000)
				}
			}
		})
	}
}
