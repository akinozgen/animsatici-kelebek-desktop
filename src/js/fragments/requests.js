const template = require('../helpers/template')

module.exports = {

    get: fragmentElement => {
        fragmentElement.load(config.fragmentViews + '/requests.html', element => {
            let fragment = $(element)

            let collapsible = $('.collapsible'),
                tooltipped = $('.tooltipped')

            collapsible.collapsible()
            tooltipped.tooltip()

            F.Requests.getRequests()
        })
    },

    getRequests: () => {
        let loggedUser = JSON.parse(localStorage.getItem('user'))
        let userid     = loggedUser.Kimlik
        let url        = config.url +'UserRelations/requests/userid/' + userid + '/referer/' + config.referer

        $.ajax({
            url: url,
            type: 'get',
            async: true,
            success: data => {
                if (data.result == 'error_cant') {
                    Materialize.toast('İstekler yüklenemedi.', 2000)
                } else if (data.result == 'error_not') {
                    Materialize.toast('Yeni istek yok.', 2000)
                } else if (data.result == 'success') {
                    localStorage.setItem('requests', JSON.stringify(data))

                    F.Requests.loadRequests()
                }
            }
        })
    },

    loadRequests: () => {
        let requests = JSON.parse(localStorage.getItem('requests'))
        let context = $('#asks'),
            itemTemplate = $('template[name="ask-list-item"]')

        for (let i = 0; i < requests.length; i++) {
            let appendTemplate = $( template.replace(itemTemplate.html(), requests[i]) )
            context.append(appendTemplate.prop('outerHTML'))
        }
    },

    answer: (answer, $this) => {
        let trigger = $($this),
            listItem = $('li[data-id="'+ trigger.data('id') +'"]'),
            loggedUser = JSON.parse(localStorage.getItem('user')),
            addingId = trigger.data('id'),
            addedId  = loggedUser.Kimlik,
            url      = config.url + 'UserRelations/answer/adding/' + addingId + '/added/' + addedId +
                       '/answeris/' + answer + '/referer/' + config.referer
        
        $.ajax({
            url: url,
            type: 'get',
            async: true,
            success: data => {
                if (data.result == 'error_cant' && data.result =='error_not') {
                    Materialize.toast('Cevap gönderilemedi.', 2000)
                } else if (data.result == 'error_exists') {
                    Materialize.toast('İstek gönderilmemiş.', 2000)
                } else if (data.result == 'success') {
                    Materialize.toast('Artık arkadaşsınız.', 2000)
                    listItem.remove()
                }
            }
        })

    }

}