const template = require('../helpers/template')
const base64   = require('base64-url')

module.exports = {
    get: (fragmentElement) => {
        fragmentElement.load(config.fragmentViews + '/friends.html', element => {
            let fragment = $(element)
            
            let collapsible = $('.collapsible'),
                tooltipped = $('.tooltipped'),
                searchFromTrigger = $('[data-activates="search-form"]'),
                searchFormInput = $('.search-form #search'),
                searchFormDisposer = $('[data-deactivates="search-form"]'),
                refreshButton = $('[data-activates="refresh"]')
                //endOfDeclarations
            
            collapsible.collapsible()
            tooltipped.tooltip()
            searchFromTrigger.click(() => {
                let searchForm = $('.search-form')
				let state = searchForm.hasClass('hide')
				console.log(state)
				if (state) {
					searchForm.addClass('hide')
				} else {
					searchForm.removeClass('hide')
				}
			})//endOfActivateSearch
            searchFormInput.keypress(F.Friends.doSearch)
            searchFormDisposer.click(($this) => {
                let searchForm = $( '.' + $($this.currentTarget).data('deactivates') )
                let searchFormInput = $(searchForm.find('input#search'))
                    searchFormInput.val('')
                    searchForm.addClass('hide')
                F.Friends.loadFriends('friends')
            })//endOfSearchFormDisposer
            refreshButton.click(F.Friends.getFriends)//refreshButton onClick declaration

            F.Friends.getFriends()
        })
    },

    getFriends: () => {
        let loggedUser = JSON.parse(localStorage.getItem('user'))
        let url = config.url + 'UserRelations/get/userid/' + loggedUser.Kimlik + '/referer/' + config.referer

        $.ajax({
            url: url,
            type: 'get',
            async: true,
            success: data => {
                if (data.result == 'error_cant') {
                    Materialize.toast('Arkadaşlar güncellenemedi.', 2000)
                } else if (data.result == 'error_not') {
                    Materialize.toast('Hiç arkadaşınız yok.', 2000)
                } else {
                    localStorage.setItem('friends', JSON.stringify(data))
                    F.Friends.loadFriends('friends')
                }
            }
        })
    },

    loadFriends: (key) => {
        let data = JSON.parse(localStorage.getItem(key))
        let loggedUser = JSON.parse(localStorage.getItem('user'))

        let context = $('#friends')
        let itemTemplate = $('template[name="friend-list-item"]')

        context.html('')

        for (let i = 0; i < data.length; i++) {
            if (data[i].Kimlik == loggedUser.Kimlik)
                continue

            let appendTemplate = $( template.replace(itemTemplate.html(), data[i]) )
            context.append(appendTemplate.prop('outerHTML'))
        }

        if (context.html() == "") {
            context.html('<center style="color: #e0e0e0">Sonuç bulunamadı.</center>')
        }
    },

    doSearch: $this => {
        let val = $($this.currentTarget).val() + String.fromCharCode($this.keyCode)
        let url = config.url + 'UserRelations/search/query/' +
                  String(base64.encode(val)).replace('/', 'SLASH') + 
                  '/referer/' + config.referer
        
        if (val.length < 4) {
            F.Friends.loadFriends('friends')
            return
        }

        $.ajax({
            url: url,
            type: 'get',
            async: true,
            success: data => {
                if (data.result == 'error_cant') {
                    Materialize.toast('Arama tamamlanamadı.', 2000)
                } else if (data.result == 'error_not') {
                    Materialize.toast('Sonuç bulunamadı.', 2000)
                } else {
                    localStorage.setItem('users_search_result', JSON.stringify(data.data))
                    F.Friends.loadFriends('users_search_result')
                }
            }
        })
    },

    askForFriendship: $this => {
        let loggedUser = JSON.parse(localStorage.getItem('user'))
        let addedId    = $($this).data('id')
        let addingId   = loggedUser.Kimlik
        let url        = config.url + 'UserRelations/ask/adding/' +
                         addingId + '/added/' + addedId +
                         '/referer/' + config.referer
        
        $.ajax({
            url: url,
            type: 'get',
            async: true,
            success: data => {
                if (data.result == 'error_cant' || data.result == 'error_not') {
                    Materialize.toast('Arkadaşlık isteği gönderilemedi.', 2000)
                } else if (data.result == 'error_exists') {
                    Materialize.toast('Zaten arkadaşsınız.', 2000)
                } else if (data.result == 'error_not_exists') {
                    Materialize.toast('Zaten istek gönderilmiş.', 2000)
                } else if (data.result == 'success') {
                    Materialize.toast('Arkadaşlık isteği gönderildi...')
                }
            }
        })
    },

    removeFriendship: $this => {
        let loggedUser = JSON.parse(localStorage.getItem('user'))
        let removing   = loggedUser.Kimlik
        let removed    = $($this).data('id')
        let url        = config.url + 'UserRelations/remove/referer/' +
                         config.referer + '/removing/' +
                         removing + '/removed/' + removed,
            listItem   = $('li[data-id="'+ removed +'"]')

       let confirm     = window.confirm('Bu kişiyi arkadaşlıktan çıkarılacak. Devam edilsin mi ?')

       if (!confirm)
        return

        $.ajax({
            url: url,
            type: 'get',
            async: true,
            success: data => {
                if (data.result == 'error_cant' || data.result == 'error_not') {
                    Materialize.toast('Arkadaşlık silinemedi.', 2000)
                } else if (data.result == 'error_exists') {
                    Materialzie.toast('Zaten arkadaş değilsiniz.', 2000)
                } else if (data.result == 'success') {
                    Materialize.toast('Artık arkadaş değilsiniz.', 200)
                    listItem.remove()
                }
            }
        })
    },

    viewProfile: $this => {
        let userId     = $($this).data('id')
        let cachedUser = JSON.parse(localStorage.getItem('user_' + userId))
        let url        = config.url + 'User/profile/userid/' + userId + '/referer/' + config.referer
        let modal      = $('#profileModal').modal()

        $.ajax({
            url: url,
            type: 'get',
            async: true,
            success: data => {
                if (data.result == 'error_cant') {
                    Materialize.toast('Profil yüklenemedi.', 2000)
                } else if (data.result == 'error_not') {
                    Materialize.toast('Profil bulunamadı.', 2000)
                } else if (data.result == 'success') {
                    let pp = modal.find('.pp'),
                        name = modal.find('.name'),
                        friends = modal.find('.friends'),
                        groups = modal.find('.groups'),
                        notes = modal.find('.notes')
                    name.text(data.AdSoyad)
                    friends.text(data.Arkadaslar)
                    notes.text(data.Notlar)
                    groups.text(data.Gruplar)
                    pp.attr('src', data.Avatar)
                    modal.modal('open')
                }
            }
        })
    }
}