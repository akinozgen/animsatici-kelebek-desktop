let resolve = dir => {
    let hashRaw = window.location.hash.replace('#', '')

    let hashSplit = String(hashRaw).split('/')
    let hash = params = []
    
    for (let i = 0; i < hashSplit.length; i++) {
        if (hashSplit[i].includes(':'))
        {
            let split = hashSplit[i].split(':')
            params[split[0]] = split[1]
            continue
        }
        hash.push(hashSplit[i])
    }
    let controller, method

    if (hash[0] != "") {
        controller = require(dir + '/' + config.controllers+'/'+hash[0]+'.js')
    } else {
        controller = require(dir + '/' + config.controllers+'/'+config.default_controller+'.js')
    }

    if (hash[1]) {
        method = controller[hash[1]]
    } else {
        method = controller[config.default_method]
    }
    
    method(params);
}

module.exports = resolve