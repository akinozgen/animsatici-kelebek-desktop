const notification = require('electron-notifications')

let backgroundWorker = {
    /**
     * she creates an interval for checking server
     * notifications status. for clear: bw.checkForFriendRequests(oldInterval, false)
     * 
     * @param {number|interval} [interval=10000] 
     * @param {boolean} [start=true] 
     * @returns {interval|null}
     */
    checkForFriendRequests: (interval = 10000, start = true, callback) => {
        if ( ! start) {
            clearInteval(interval)
            return
        }

        let timeout = setInterval(() => {
            let loggedUser = JSON.parse(localStorage.getItem('user'))
            $.ajax({
                url: config.url + 'UserRelations/requests/userid/' + loggedUser.Kimlik + '/referer/' + config.referer,
                type: 'get',
                async: true,
                success: data => {
                    if ( data.result == "success" ) {
                        callback(data.data)
                    }
                }
            })
        }, interval)

        return timeout
    },
    /**
     * she creates an interval for checking server
     * notifications status. for clear: bw.checkForFriendRequests(oldInterval, false)
     * 
     * @param {number|interval} [interval=10000] 
     * @param {boolean} [start=true] 
     * @returns {interval|null}
     */
    checkForSharedNotes: (interval = 10000, start = true, callback) => {
        if ( ! start) {
            clearInterval(interval)
            return
        }

        let timeout = setInterval(() => {
            let loggedUser = JSON.parse(localStorage.getItem('user'))
            $.ajax({
                url: config.url + 'Notes/get_notes/userid/' + loggedUser.Kimlik + '/referer/' + config.referer + '/table/' + loggedUser.NotlarTablosu,
                type: 'get',
                async: true,
                success: data => {
                    if ( data.result == "success" ) {
                        callback(data.data)
                    }
                }
            })
        }, interval)

        return timeout
    },
    /**
     * @param {string[]} payload 
     * @returns {null|notification}
     */
    showNotification: payload => {
        let showedNotifications = JSON.parse(localStorage.getItem('notified_notifications')) ? 
                                  JSON.parse(localStorage.getItem('notified_notifications')) : []

        if (showedNotifications.indexOf(payload.uniqId) >= 0) 
            return null
        
        showedNotifications.push(payload.uniqId)
        
        localStorage.setItem('notified_notifications', JSON.stringify(showedNotifications))

        return notification.notify(payload.title, {
            message: payload.message,
            buttons: payload.buttons,
            icon: payload.icon
        })
    },

    /**
     * @param {notification} notification 
     * @param {function}     callback 
     */
    handleNotification: (notification, callback) => {
        notification.on('buttonClicked', callback)
    }

}

module.exports = backgroundWorker