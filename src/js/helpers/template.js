module.exports = {
    /**
     * @var string viewName
     */
    getView: (viewName) => {
        this.defer = $.Deferred();
        $.ajax({
            url: config.views + '/' + viewName + '.html',
            type: 'get',
            data: [],
            success: data => {
                this.defer.resolve(data);
            }
        });

        this.render = (callback) => {
            this.defer.promise().then((data) => {
                content = callback(data)
                
                $('body').html(content)
                
                if (typeof G.onDomChanged !== 'undefined') {
                    G.onDomChanged()
                }
            })
        }
        return this
    },

    /**
     * @var string context
     * @var object data
     */
    replace: (context, data) => {
        Object.keys(data).forEach(key => {
            context = context.split('{{ ' + key + ' }}').join(data[key])
        })
        
        return context
    }

}