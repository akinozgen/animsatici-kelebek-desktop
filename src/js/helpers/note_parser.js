module.exports = {
    parse: (noteData) => {
        
        switch (noteData['Renk']) {
            case '0':  noteData['Renk'] = 'grey darken-4'; break;
            case '1':  noteData['Renk'] = 'grey darken-2'; break;
            case '2':  noteData['Renk'] = 'green darken-1'; break;
            case '3':  noteData['Renk'] = 'light-blue'; break;
            case '4':  noteData['Renk'] = 'red darken-1'; break;
            case '5':  noteData['Renk'] = 'purple lighten-1'; break;
            case '6':  noteData['Renk'] = 'pink lighten-1'; break;
            case '7':  noteData['Renk'] = 'orange darken-1'; break;
            case '8':  noteData['Renk'] = 'brown lighten-1'; break;
            case '9':  noteData['Renk'] = 'grey lighten-2'; break;
            case '10': noteData['Renk'] = 'grey lighten-5'; break;
        }

        let loggedUser = JSON.parse(localStorage.getItem('user'))

        noteData['Icon'] = ''

        if (noteData['Paylasilan']) {
            noteData['Icon'] = 'people_outline'
        } if (noteData['Sahibi'] != loggedUser.Kimlik) {
            noteData['Icon'] = 'lock'
            noteData['SahibiAdSoyad'] = '<i class="material-icons">person</i><span>'+ noteData['SahibiAdSoyad'] +'</span>'
        } else {
            noteData['SahibiAdSoyad'] = ''
        }

        if (noteData['Hatirlat']) {
            noteData['Hatirlatici'] = '<i class="material-icons">alarm</i><span>'+ noteData['Hatirlatici'] +'</span>'
        } else {
            noteData['Hatirlatici'] = ''
        }

        return noteData
    }
}