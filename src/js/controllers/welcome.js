let templateHelper = require('../helpers/template.js')

let checkLogin = () => {
    if (localStorage.getItem('login_state') == 'true') {
        window.location.hash = 'home'
    }
}

let main = () => {
    let view = templateHelper.getView('welcome')
    view.render((param) => {
        return param
    })

    $('body').change((x) => {
        
    })

    checkLogin()
}

module.exports = {
    "main": main,
    "checkLogin": checkLogin
}