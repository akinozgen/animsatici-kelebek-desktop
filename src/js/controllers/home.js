// usings
const backgroundWorker = require('../helpers/backgroundWorker')
const { remote }       = require('electron')
let templateHelper     = require('../helpers/template')
let welcomeController  = require('./welcome')
// fragments usings
let notes    = require('../fragments/notes')
let groups   = require('../fragments/groups')
let friends  = require('../fragments/friends')
let profile  = require('../fragments/profile')
let settings = require('../fragments/settings')
let requests = require('../fragments/requests')

// Fragment declarations
F.Notes    = notes
F.Groups   = groups
F.Friends  = friends
F.Profile  = profile
F.Settings = settings
F.Requests = requests

// 3rd party plugin declarations for ex. jQuery
G.onDomChanged = () => {
	let collapse = $('.collapse'), fragmentFrame = $('[data-content-area=fragmentFrame]')
	let dropdownButton = $(".dropdown-button")

	dropdownButton.dropdown()

	collapse.sideNav({
		edge: 'left',
		closeOnClick: true,
		draggable: true
	})

	F.Notes.get(fragmentFrame) // setting up default fragment
}

// Globals functions for inView usage
G.Home = {
	logOut: () => {
		if (confirm('Çıkış yapmak istediğinize emin misiniz ?')) {
			localStorage.removeItem('login_state')
			localStorage.removeItem('user')
			window.location = '#welcome'
		}
	}
}

// Controller methods
// Main method (default method of controller)
let main = () => {
	let loggedUser = JSON.parse(localStorage.getItem('user'))
	let view = templateHelper.getView('home')
	view.render((param) => {
		content = templateHelper.replace(param, {
			'app_name': config.app_name,
			'avatar': loggedUser.Avatar,
			'name': loggedUser.AdSoyad,
			'email': loggedUser.Eposta
		})

		return content
	})

	backgroundWorker.checkForFriendRequests(10000, true, (requests) => {
		if ( ! (requests.length > 0) )
			return
		let loggedUser = JSON.parse(localStorage.getItem('user'))
		for (let i = 0; i < requests.length; i++) {
			let req = requests[i]

			let payload = {
				uniqId: req.Kimlik + loggedUser.Kimlik,
				title: '1 Yeni Arkadaşlık İsteği',
				message: req.AdSoyad + ' sizinle arkadaş olmak istiyor.',
				buttons: ['Görüntüle', 'Kapat'],
				icon: req.Avatar
			}

			let notification = backgroundWorker.showNotification(payload)

			if (notification != null) {
				backgroundWorker.handleNotification(notification, (buttonText, buttonIndex, options) => {
					if (buttonIndex === 0) {
						F.Requests.get($('[data-content-area=fragmentFrame]'))
						remote.BrowserWindow.getAllWindows()[0].restore()
					}
					notification.close()
				})
			}
		} 
	})
	backgroundWorker.checkForSharedNotes(10000, true, (notes) => {
		if (notes.length < 1)
			return

		let loggedUser = JSON.parse(localStorage.getItem('user'));
		let showedNotifications = JSON.parse(localStorage.getItem('notified_notes') ?
											 localStorage.getItem('notified_notes') : "[]");

		for (let i = 0; i < notes.length; i++) {
			let note = notes[i];
			
			if ((note.Sahibi != loggedUser.Kimlik) && (note.Paylasilan == true) && (showedNotifications.map(x => x.Kimlik == note.Kimlik)))
			{
				let payload = {
					uniqId: note.Kimlik + Date.now(),
					title: "1 yeni paylaşılan not.",
					message: note.SahibiAdSoyad + " sizinle bir not paylaştı.",
					buttons: ['Görüntüle', 'Kapat']
				}
				console.log(payload);
				let notification = backgroundWorker.showNotification(payload);

				if (notification != null) {
					backgroundWorker.handleNotification(notification, (buttonText, buttonIndex, optinos) => {
						if (buttonIndex === 0) {
							remote.BrowserWindow.getAllWindows()[0].restore()
							F.Notes.get($('[data-content-area=fragmentFrame]'))
						}
						notification.close();
					})
				}
			}
		}
	});
}

// Registering module
module.exports = {
	'main': main
}