let templateHelper = require('../helpers/template')
const base64       = require('base64-url')

G.Register = {
    attempToRegister: (name, email, password) => {
        $.ajax({
            url: config.url + 'User/register'+
            '/email/' + email + '/name/' + base64.encode(name) + '/password/' + 
            password + '/referer/' + config.referer,
            type: 'get',
            success: (data) => {
                if (data.result != 'success') {
                    alert("Kayıt başarısız. Daha sonra tekrar dene.")
                } else {
                    localStorage.setItem('login_state', true)
                    localStorage.setItem('user', JSON.stringify(data.data))

                    window.location = '#home'
                }
            }
        })
    }
}

let main = () => {
    let view = templateHelper.getView('register')
     view.render((param) => {
        return param
    })
}

module.exports = {
    'main': main
}