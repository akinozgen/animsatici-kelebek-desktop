let templateHelper = require('../helpers/template')
let welcomeController = require('./welcome')

G.Login = {
    'attempToLogin': (email, password) => {
        $.ajax({
            url: config.url + "User/login/email/"+email+"/password/"+password+"/referer/"+config.referer,
            type: 'get',
            success: data => {
                if (data.result == 'success') {
                    localStorage.setItem('login_state', true)
                    localStorage.setItem('user', JSON.stringify(data.data))

                    window.location = '#home'
                } else {
                    alert('E-Posta veya şifre hatalı.')
                }
            }
        })
    }
}

let main = () => {
    welcomeController.checkLogin()
    let view = templateHelper.getView('login')
    view.render((param) => {
        return param
    })
}

module.exports = {
    "main": main
}
