module.exports = function(grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        less: {
            development: {
                options: {
                    comments: true,
                    sourceMap: true,
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "src/css/main.css": [
                        "src/less/main.less"
                    ] // destination file
                }
            }
        },
        watch: {
            options: {
                livereloadOnError: true
            },
            styles: {
                files: ['src/less/*.less'],
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.option('force', true);
    grunt.registerTask('default', ['less', 'watch']);
};